/*
 * layers.h
 *
 * Created: 3/28/2021 1:17:28 PM
 *  Author: amund
 */ 


#ifndef LAYERS_H_
#define LAYERS_H_

#include "leds.h"
#include <stdint.h>
#include "config.h"

#define MAX_KEYS 42
#define MAX_MACRO_LENGTH 64

#define HID_LT 100

// Define the types of keys.
typedef enum {
	KEY_NORMAL,
	KEY_MODIFIER,
	KEY_LAYER,
	KEY_MACRO
} key_type;

// Define the structure of a key.
typedef struct {
	key_type type;
	uint8_t code;
	color_t color;
} key_t;

// Define the structure of a layer.
typedef struct{
	key_t keys[MAX_KEYS];
} layer_t;

// Define the structure of a macro.
typedef struct {
	uint8_t modifiers[MAX_MACRO_LENGTH];
	uint8_t keys[MAX_MACRO_LENGTH];
	uint8_t length_mod;
	uint8_t length_keys;
} macro_t;

extern macro_t macros[];


// See usb_protocol_hid.h for key mapping

extern const layer_t layer_0;
extern const layer_t layer_1;

extern const layer_t* layers[];

enum {
	LAYER0,
	LAYER1,
	NUM_LAYERS // Count of layers
};

#endif /* LAYERS_H_ */