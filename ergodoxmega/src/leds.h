/*
 * neopixel_spi.c
 *
 * 
 * Author: amundaune, based on code from @printedcircuitbirds (git@gitlab.com:printedcircuitbirds/libavr_neopixel_spi.git)
 */

#ifndef LEDS_H_
#define LEDS_H_

#include <asf.h>

/* 
 * Timing defines for the transmission of one SPI byte per Neopixel bit
 *
 * Source: https://wp.josh.com/2014/05/13/ws2812-neopixels-are-not-so-finicky-once-you-get-to-know-them/
 * Zero high voltage time: Min 200ns, Max 500ns
 * One high voltage time: Min 550ns
 * Zero/One low voltage time: Min 450ns, Max 5000ns
 *
 * For F_CPU = 20 or 10 MHz => 625 kbit/s = ~26k RGB/s or ~20k RGBW/s
 *   zero bit = 25% duty SPI byte = 8bit/(5MHz) * 25% = 400ns high, 1200ns low
 *   one bit  = 50% duty SPI byte = 8bit/(5MHz) * 50% = 800ns high, 800ns low
 * For F_CPU = 16 or 8 MHz => 500 kbit/s = ~21k RGB/s or ~16k RGBW/s
 *   zero bit = 25% duty SPI byte = 8bit/(4MHz) * 25% = 500ns high, 1500ns low (very close to the limit!)
 *   one bit  = 50% duty SPI byte = 8bit/(4MHz) * 50% = 1000ns high, 1000ns low
 */

#define LED_SPI_PORT		PORTD
#define LED_DATA_PIN		PIN5_bm
#define LED_DATA_PINCTRL	PIN5CTRL
#define LED_SPI				SPID
#define NEOPIXEL_ZERO		((uint8_t) ~0xc0)
#define NEOPIXEL_ONE		((uint8_t) ~0xf0)
#define NEOPIXEL_TYPE_RGB
#define HSB_MAX_HUE			1536

/*
 * The data type used for configuring the LEDs
 */
#define NEOPIXEL_LED_BYTES   (3)


/*
 * Data type for the hue functions
 */
typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;

typedef union {
	struct {
		uint8_t g;
		uint8_t r;
		uint8_t b;
	};
	struct {
		uint32_t channel: 24;
	};
	uint8_t array[3];
} color_t;


/*
 * Private functions
 */

// static void neopixel_spi_send_byte(uint8_t byte);

/*
 * Public functions
 */

void neopixel_spi_init(void);
void neopixel_spi_configure_single(color_t color);
void neopixel_spi_configure_array(color_t *array, uint16_t length);
color_t hsb2rgb(hsb_t config);
color_t rotate_pixel_hue(uint8_t value, uint8_t brightness);
void neopixel_configure_constant(color_t color, uint16_t length);
void neopixel_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t length);
void neopixel_configure_off(uint16_t length);
void neopixel_configure_off_and_single(color_t color, uint16_t position, uint16_t length);


#define CLR_FIRE   { .g=0x1f, .r=0x5f, .b=0x00 }
#define CLR_AZUL   { .g=0x5f, .r=0x00, .b=0x1f }
#define CLR_ORANGE { .g=0x13, .r=0x5f, .b=0x00 }
#define CLR_YELLOW { .g=0x2f, .r=0x3f, .b=0x00 }
#define CLR_PURPLE { .g=0x00, .r=0x6f, .b=0x2f }
#define CLR_BLUE   { .g=0x00, .r=0x00, .b=0x7f }
#define CLR_GREEN  { .g=0x6f, .r=0x00, .b=0x00 }
#define CLR_RED    { .g=0x00, .r=0xef, .b=0x00 }
#define CLR_NONE   { .g=0x00, .r=0x00, .b=0x00 }


#endif /* LEDS_H_ */