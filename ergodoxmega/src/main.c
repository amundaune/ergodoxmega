/*
 * layers.h
 *
 * Created: 3/28/2021 1:17:28 PM
 *  Author: amund
 */ 

#include "main.h"
#include <asf.h>
#include <util/delay.h>

static volatile bool main_b_kbd_enable = false;

#define NUM_KEYS		NUM_COL*6
#define NUM_LEDS		NUM_KEYS+3
#define COLUMN_PORT		PORTA

#if defined(RIGHT_KEYBOARD)
#define NUM_COL			7
#define COLUMN_gm		(PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm)
uint8_t led_mapping[NUM_KEYS] = {3 , 4 , 5 , 6 , 7 , 8 , 9 , 16, 15, 14, 13, 12, 11, 10, 17, 18, 19, 20, 21, 22, 23, 30, 29, 28, 27, 26, 25, 24, 31, 32, 33, 34, 35, 36, 37, 44, 43, 42, 41, 40, 39, 38};
#elif defined(LEFT_KEYBOARD)
#define NUM_COL			6
#define COLUMN_gm		(PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm)
uint8_t led_mapping[NUM_KEYS] = {3 , 4 , 5 , 6 , 7 , 8 , 14 , 13, 12, 11, 10, 9, 15, 16, 17, 18, 19, 20, 26, 25, 24, 23, 22, 21, 27, 28, 29, 30, 31, 32, 38, 37, 36, 35, 34, 33};
#endif

color_t led_colors[NUM_KEYS+3];


volatile uint8_t x_axis = 0x00;
volatile uint8_t key_pressed = 0;
volatile uint8_t key_to_update = 0xFF;
volatile uint8_t layer_selector = 0;
volatile uint8_t selected_layer = 1;

key_t current_layer[NUM_KEYS];
volatile uint8_t new_key_state[NUM_KEYS];
volatile uint8_t curr_key_state[NUM_KEYS];


int main(void)
{
	irq_initialize_vectors();
	cpu_irq_enable();

	sleepmgr_init();
	sysclk_init();
	
	// Initialize GPIO ports for keys and LEDs
	PORT_init();
	// Initialize SPI for Neopixel LED control
	neopixel_spi_init();
	// Set the initial keyboard layer
	select_new_layer();
	// Reset the state of all keys
	reset_key_states();

	// Start USB stack to authorize VBus monitoring
	udc_start();
	
	while (true) {
		// Scan the state of all keys
		scan_keys();
		// Check if any keys' states need to be updated
		look_for_updates();
		// Update the current layer if the layer selector has changed
		select_new_layer();
		// Put the system to sleep to save power
		sleepmgr_enter_sleep();
	}
}

// Update the current keyboard layer to the given layer
void update_current_layer(layer_t* layer_to_update)
{
	for (uint8_t i = 0; i < NUM_KEYS; i++)
	{
		current_layer[i].type = layer_to_update->keys[i].type;
		current_layer[i].code = layer_to_update->keys[i].code;
		current_layer[i].color = layer_to_update->keys[i].color;
		led_colors[led_mapping[i]] = current_layer[i].color;
	}
}

// Initialize GPIO ports for key scanning and LED control
void PORT_init()
{
	// Enable pullups for columns
	PORTCFG.MPCMASK = COLUMN_gm;
	COLUMN_PORT.PIN0CTRL = PORT_OPC_PULLDOWN_gc | PORT_ISC_BOTHEDGES_gc;
	
	// Configure row outputs
	PORTA.DIRSET = PIN7_bm;
	PORTD.DIRSET = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm;
	// Set row outputs low
	PORTA.OUTCLR = PIN7_bm;
	PORTD.OUTCLR = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm;
}

// Update the current layer if the layer selector has changed
void select_new_layer()
{
	if (layer_selector != selected_layer)
	{
		// Update current layer and LED color based on the layer selector
		if (layer_selector == LAYER0)
		{
			update_current_layer(layers[LAYER0]);
			led_colors[0] = led_colors[1] = led_colors[2] = (color_t)CLR_FIRE;
		} else if (layer_selector == LAYER1)
		{
			update_current_layer(layers[LAYER1]);
			led_colors[0] = led_colors[1] = led_colors[2] = (color_t)CLR_AZUL;
		}
		neopixel_spi_configure_array(led_colors, NUM_KEYS+3);
		selected_layer = layer_selector;
	}
}

// Reset the state of all keys to unpressed
void reset_key_states()
{
	for (uint8_t i = 0; i < NUM_KEYS; i++)
	{
		new_key_state[i] = 0;
		curr_key_state[i] = 0;
	}
}

// Scan the state of all keys
void scan_keys(void)
{
	// Array that stores the row control pins
	uint8_t row_pins[6] = {PIN7_bm, PIN0_bm, PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm};
	
	// Keep track of which row we're on
	uint8_t row_counter = 0;
	
    // Initialize all rows to low state.
    PORTA.OUTCLR = PIN7_bm;
    PORTD.OUTCLR = PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm;
	
	// Loop through each row
	for (uint8_t i = 0; i < 6; i++)
	{
		// Based on the row index, set the appropriate row pin high
		if (i == 0)
		{
			PORTA.OUTSET = row_pins[i];
		} else {
			PORTD.OUTSET = row_pins[i];
		}
			
		// Read the column inputs
		volatile uint8_t x_axis = COLUMN_PORT.IN & COLUMN_gm;
					
		for (uint8_t j = 0; j < NUM_COL; j++)
		{
			// If a key is pressed, increase its state count
			// If a key is released, reset its state count
			// Debouncing is handled by the state count
			if (x_axis & (1 << j))
			{
				if (new_key_state[row_counter + j] == 255)
				{
					new_key_state[row_counter + j] = 1;
				} else {
					new_key_state[row_counter + j] += 1;
				}
			} else {
				new_key_state[row_counter + j] = 0;
			}
		}
		
		// Reset the apropriate row pin to low
		if (i == 0) 
		{
			PORTA.OUTCLR = row_pins[i];
		} else {
			PORTD.OUTCLR = row_pins[i];
		}
		
		row_counter += NUM_COL;
	}
}

// Check for any key whose state needs to be updated
void look_for_updates()
{
	for (uint8_t i = 0; i < NUM_KEYS; i++)
	{
        // If a key is pressed or released, mark it for update
        // Debouncing is implemented by only processing keys after their state count passes a certain threshold
		if (((curr_key_state[i] == 0) && (new_key_state[i] > 30)) || ((curr_key_state[i] > 0) && (new_key_state[i] == 0)))
		{
			key_to_update = i;
			break;
		}
	}
}

// Update the state of the key marked for update
void update_key()
{	
	if (key_to_update != 0xFF)
	{
		bool success = false;
		
        // Handle different types of keys: modifier, layer, and others
        // If the key is pressed, send the appropriate HID report to the host
		
		switch (current_layer[key_to_update].type)
		{
			case KEY_MODIFIER:
				if(new_key_state[key_to_update] == 0)
				{
					success = udi_hid_kbd_modifier_up(current_layer[key_to_update].code);
				} else
				{
					success = udi_hid_kbd_modifier_down(current_layer[key_to_update].code);
				}
				break;
				
			case KEY_LAYER:
				if (!(new_key_state[key_to_update] == 0))
				{
					if (layer_selector == (NUM_LAYERS - 1))
					{
						layer_selector = 0;
					} else
					{
						layer_selector += 1;
					}
				}
				success = true;
				break;
			
			case KEY_MACRO:
			{
				// Get the macro associated with this key
				macro_t* macro = &macros[current_layer[key_to_update].code];
				
				if (!(new_key_state[key_to_update] == 0))
				{
					
					// Send each keycode in the macro
					for (uint8_t i = 0; i < macro->length_mod; i++)
					{
						success &= udi_hid_kbd_modifier_down(macro->modifiers[i]);
					}
					for (uint8_t i = 0; i < macro->length_keys; i++)
					{
						success &= udi_hid_kbd_down(macro->keys[i]);
					}
				} else {
					// Send each keycode in the macro
					for (uint8_t i = 0; i < macro->length_mod; i++)
					{
						success &= udi_hid_kbd_modifier_up(macro->modifiers[i]);
					}
					for (uint8_t i = 0; i < macro->length_keys; i++)
					{
						success &= udi_hid_kbd_up(macro->keys[i]);
					}
				}
				break;
			}
				
			default:
				if(new_key_state[key_to_update] == 0)
				{
					success = udi_hid_kbd_up(current_layer[key_to_update].code);
				} else
				{
					success = udi_hid_kbd_down(current_layer[key_to_update].code);
				}
		}
		if (success)
		{
			curr_key_state[key_to_update] = new_key_state[key_to_update];
			key_to_update = 0xFF;
		}
	}
}


void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_kbd_enable)
		return;
	update_key();
}

void main_remotewakeup_enable(void)
{
	ui_wakeup_enable();
}

void main_remotewakeup_disable(void)
{
	ui_wakeup_disable();
}

bool main_kbd_enable(void)
{
	main_b_kbd_enable = true;
	return true;
}

void main_kbd_disable(void)
{
	main_b_kbd_enable = false;
}