/*
 * layers.c
 *
 * Created: 26.06.2023 15:54:36
 *  Author: M19892
 */ 

#include <asf.h>
#include "layers.h"

// Define the number of macros.
#define NUM_MACROS 18
// Define the macros.
macro_t macros[NUM_MACROS] = {
/*
 *	GENERAL WINDOWS SHORTCUTS
 */

	{ // WIN + R
		.modifiers = {HID_MODIFIER_RIGHT_UI},
		.keys = {HID_R},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // WIN + E
		.modifiers = {HID_MODIFIER_RIGHT_UI},
		.keys = {HID_E},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // WIN + L
		.modifiers = {HID_MODIFIER_RIGHT_UI},
		.keys = {HID_L},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // WIN + CTRL + LEFT
		.modifiers = {HID_MODIFIER_RIGHT_UI, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_LEFT},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // WIN + CTRL + RIGHT
		.modifiers = {HID_MODIFIER_RIGHT_UI, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_RIGHT},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // WIN + CTRL + D
		.modifiers = {HID_MODIFIER_RIGHT_UI, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_D},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // WIN + CTRL + F4
		.modifiers = {HID_MODIFIER_RIGHT_UI, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_F4},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // ALT + F4
		.modifiers = {HID_MODIFIER_LEFT_ALT},
		.keys = {HID_F4},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // Move window to next screen
		.modifiers = {HID_MODIFIER_LEFT_SHIFT, HID_MODIFIER_RIGHT_UI},
		.keys = {HID_RIGHT},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // Move window to previous screen
		.modifiers = {HID_MODIFIER_LEFT_SHIFT, HID_MODIFIER_RIGHT_UI},
		.keys = {HID_LEFT},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // Move window to next slot
		.modifiers = {HID_MODIFIER_RIGHT_UI},
		.keys = {HID_RIGHT},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // Move window to previous slot
		.modifiers = {HID_MODIFIER_RIGHT_UI},
		.keys = {HID_LEFT},
		.length_mod = 1,
		.length_keys = 1
	},
/*
 *	IDE Commands
 */
	{ // Program device without debugging
		.modifiers = {HID_MODIFIER_LEFT_ALT, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_F5},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // Rebuild
		.modifiers = {HID_MODIFIER_LEFT_ALT, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_F7},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // Device Programming Dialog
		.modifiers = {HID_MODIFIER_LEFT_SHIFT, HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_P},
		.length_mod = 2,
		.length_keys = 1
	},
	{ // Debug and break
		.modifiers = {HID_MODIFIER_LEFT_ALT},
		.keys = {HID_F5},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // Tab right
		.modifiers = {HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_2},
		.length_mod = 1,
		.length_keys = 1
	},
	{ // Tab left
		.modifiers = {HID_MODIFIER_LEFT_CTRL},
		.keys = {HID_1},
		.length_mod = 1,
		.length_keys = 1
	}
};

enum {
	WIN_R,
	WIN_E,
	LCK_PC,
	LEFT_DSKTP,
	RIGHT_DSKTP,
	NEW_DSKTP,
	DEL_DSKTP,
	CLS_WDW,
	WDW_NXT_SCRN,
	WDW_PRV_SCRN,
	WDW_NXT_SLT,
	WDW_PRV_SLT,
	PROG_DEV,
	REBUILD,
	DEV_PROG_DIA,
	DBG_BRK,
	TAB_RGHT,
	TAB_LFT
};


#if defined(RIGHT_KEYBOARD)

/*
RIGHT KEYBOARD PATTERN

						0		1		2		3		4		5		6
	
						7		8		9		10		11		12		13
				41
						14		15		16		17		18		19		20
				28
						21		22		23		24		25		26		27
	
								29		30		31		32		33		34
			36
		35				40
			37		39
				38
								


*/

const layer_t layer_0 = {
	.keys =
	{
		{KEY_NORMAL, HID_6, CLR_FIRE},						{KEY_NORMAL, HID_7, CLR_FIRE},							{KEY_NORMAL, HID_8, CLR_FIRE},						{KEY_NORMAL, HID_9, CLR_FIRE},		{KEY_NORMAL, HID_0, CLR_FIRE},						{KEY_NORMAL, HID_UNDERSCORE, CLR_PURPLE},			{KEY_NORMAL, HID_PLUS, CLR_PURPLE},					// 0 - 6h
		{KEY_NORMAL, HID_Y, CLR_FIRE},						{KEY_NORMAL, HID_U, CLR_FIRE},							{KEY_NORMAL, HID_I, CLR_FIRE},						{KEY_NORMAL, HID_O, CLR_FIRE},		{KEY_NORMAL, HID_P, CLR_FIRE},						{KEY_NORMAL, HID_OPEN_BRACKET, CLR_FIRE},			{KEY_NORMAL, HID_CLOSE_BRACKET, CLR_PURPLE},		// 7 - 13
		{KEY_NORMAL, HID_H, CLR_FIRE},						{KEY_NORMAL, HID_J, CLR_FIRE},							{KEY_NORMAL, HID_K, CLR_FIRE},						{KEY_NORMAL, HID_L, CLR_FIRE},		{KEY_NORMAL, HID_COLON, CLR_FIRE},					{KEY_NORMAL, HID_QUOTE, CLR_FIRE},					{KEY_NORMAL, HID_LEFT, CLR_AZUL},					// 14 - 20
		{KEY_NORMAL, HID_N, CLR_FIRE},						{KEY_NORMAL, HID_M, CLR_FIRE},							{KEY_NORMAL, HID_COMMA, CLR_PURPLE},				{KEY_NORMAL, HID_DOT, CLR_PURPLE},	{KEY_NORMAL, HID_SLASH, CLR_PURPLE},				{KEY_NORMAL, HID_BACKSLASH, CLR_PURPLE},			{KEY_NORMAL, HID_RIGHT, CLR_AZUL},					// 21 - 27
		{KEY_NORMAL, HID_END, CLR_AZUL},					{KEY_MODIFIER, HID_MODIFIER_RIGHT_SHIFT, CLR_GREEN},	{KEY_MODIFIER, HID_MODIFIER_LEFT_ALT, CLR_GREEN},	{KEY_NORMAL, HID_F9, CLR_ORANGE},	{KEY_MODIFIER, HID_MODIFIER_RIGHT_ALT, CLR_GREEN},	{KEY_MODIFIER, HID_MODIFIER_RIGHT_ALT, CLR_GREEN},	{KEY_MODIFIER, HID_MODIFIER_LEFT_CTRL, CLR_GREEN},	// 28 - 34 Leftmost function on this row corresponds to button below the Xmega device
		{KEY_MODIFIER, HID_MODIFIER_RIGHT_UI, CLR_GREEN},	{KEY_NORMAL, HID_ESCAPE, CLR_GREEN},					{KEY_NORMAL, HID_F5, CLR_ORANGE},					{KEY_LAYER, 0, CLR_RED},			{KEY_NORMAL, HID_TAB, CLR_GREEN}, 					{KEY_NORMAL, HID_SPACEBAR, CLR_GREEN},				{KEY_NORMAL, HID_HOME, CLR_AZUL}					// 35 - 41 Rightmost function on this row corresponds to button above the Xmega device
	}
};

const layer_t layer_1 = {
	.keys =
	{
		{KEY_MACRO, PROG_DEV, CLR_BLUE},					{KEY_MACRO, REBUILD, CLR_BLUE},							{KEY_MACRO, DBG_BRK, CLR_BLUE},				{KEY_NORMAL, 0, CLR_NONE},					{KEY_NORMAL, 0, CLR_NONE},						{KEY_NORMAL, 0, CLR_NONE},				{KEY_MACRO, DEV_PROG_DIA, CLR_BLUE},			// 0 - 6
		{KEY_NORMAL, HID_KEYPAD_DIVIDE, CLR_PURPLE},		{KEY_NORMAL, HID_KEYPAD_7, CLR_FIRE},					{KEY_NORMAL, HID_KEYPAD_8, CLR_FIRE},		{KEY_NORMAL, HID_KEYPAD_9, CLR_FIRE},		{KEY_NORMAL, HID_KEYPAD_MINUS, CLR_PURPLE},		{KEY_NORMAL, 0, CLR_NONE},				{KEY_NORMAL, 0, CLR_NONE},						// 7 - 13
		{KEY_NORMAL, HID_KEYPAD_MULTIPLY, CLR_PURPLE},		{KEY_NORMAL, HID_KEYPAD_4, CLR_FIRE},					{KEY_NORMAL, HID_KEYPAD_5, CLR_FIRE},		{KEY_NORMAL, HID_KEYPAD_6, CLR_FIRE},		{KEY_NORMAL, HID_KEYPAD_PLUS, CLR_PURPLE},		{KEY_NORMAL, 0, CLR_NONE},				{KEY_NORMAL, HID_LEFT, CLR_AZUL},				// 14 - 20
		{KEY_NORMAL, HID_DOT, CLR_PURPLE},					{KEY_NORMAL, HID_KEYPAD_1, CLR_FIRE},					{KEY_NORMAL, HID_KEYPAD_2, CLR_FIRE},		{KEY_NORMAL, HID_KEYPAD_3, CLR_FIRE},		{KEY_NORMAL, HID_COMMA, CLR_PURPLE},			{KEY_NORMAL, 0, CLR_NONE},				{KEY_NORMAL, HID_RIGHT, CLR_AZUL},				// 21 - 27
		{KEY_NORMAL, HID_END, CLR_AZUL},					{KEY_MODIFIER, HID_MODIFIER_RIGHT_SHIFT, CLR_GREEN},	{KEY_NORMAL, HID_0, CLR_FIRE},				{KEY_NORMAL, HID_F9, CLR_ORANGE},			{KEY_NORMAL, HID_F2, CLR_ORANGE},				{KEY_NORMAL, 0, CLR_NONE},				{KEY_MACRO, LCK_PC, CLR_BLUE},					// 28 - 34 Leftmost function on this row corresponds to button below the Xmega device
		{KEY_MODIFIER, HID_MODIFIER_RIGHT_UI, CLR_GREEN},	{KEY_NORMAL, HID_ESCAPE, CLR_GREEN},					{KEY_NORMAL, HID_F5, CLR_ORANGE},			{KEY_LAYER, 0, CLR_RED},					{KEY_NORMAL, HID_TAB, CLR_GREEN},				{KEY_NORMAL, HID_SPACEBAR, CLR_GREEN},	{KEY_NORMAL, HID_HOME, CLR_AZUL}				// 35 - 41 Rightmost function on this row corresponds to button above the Xmega device
	}
};



#elif defined(LEFT_KEYBOARD)
	
/*
LEFT KEYBOARD PATTERN

	0		1		2		3		4		5
													18
	6		7		8		9		10		11
	
			13		14		15		16		17
	12												29
			19		20		21		22		23
			
	24		25		26		27		28
														35
											32				34
												31		33
													30
*/



const layer_t layer_0 = {
	.keys = {
		{KEY_NORMAL, HID_TILDE, CLR_PURPLE},				{KEY_NORMAL, HID_1, CLR_FIRE},			{KEY_NORMAL, HID_2, CLR_FIRE},			{KEY_NORMAL, HID_3, CLR_FIRE},		{KEY_NORMAL, HID_4, CLR_FIRE},						{KEY_NORMAL, HID_5, CLR_FIRE},			// 0 - 5
		{KEY_NORMAL, HID_UP, CLR_AZUL},						{KEY_NORMAL, HID_Q, CLR_FIRE},			{KEY_NORMAL, HID_W, CLR_FIRE},			{KEY_NORMAL, HID_E, CLR_FIRE},		{KEY_NORMAL, HID_R, CLR_FIRE},						{KEY_NORMAL, HID_T, CLR_FIRE},			// 6 - 11
		{KEY_NORMAL, HID_DOWN, CLR_AZUL},					{KEY_NORMAL, HID_A, CLR_FIRE},			{KEY_NORMAL, HID_S, CLR_FIRE},			{KEY_NORMAL, HID_D, CLR_FIRE},		{KEY_NORMAL, HID_F, CLR_FIRE},						{KEY_NORMAL, HID_G, CLR_FIRE},			// 12 - 17
		{KEY_NORMAL, HID_PAGEUP, CLR_AZUL},					{KEY_NORMAL, HID_Z, CLR_FIRE},			{KEY_NORMAL, HID_X, CLR_FIRE},			{KEY_NORMAL, HID_C, CLR_FIRE},		{KEY_NORMAL, HID_V, CLR_FIRE},						{KEY_NORMAL, HID_B, CLR_FIRE},			// 18 - 23 Leftmost function on this row corresponds to button above the Xmega device
		{KEY_MODIFIER, HID_MODIFIER_RIGHT_SHIFT, CLR_GREEN},{KEY_NORMAL, HID_LT, CLR_FIRE},	{KEY_NORMAL, HID_F4, CLR_ORANGE},		{KEY_NORMAL, HID_F7, CLR_ORANGE},	{KEY_MODIFIER, HID_MODIFIER_LEFT_CTRL, CLR_GREEN},	{KEY_NORMAL, HID_PAGEDOWN, CLR_AZUL},	// 24 - 29 Rightmost function on this row corresponds to button belowthe Xmega device 
		{KEY_LAYER, 0, CLR_RED},							{KEY_NORMAL, HID_ENTER, CLR_GREEN},		{KEY_NORMAL, HID_BACKSPACE, CLR_GREEN},	{KEY_NORMAL, HID_F10, CLR_ORANGE},	{KEY_NORMAL, HID_F11, CLR_ORANGE},					{KEY_NORMAL, HID_DELETE, CLR_GREEN}		// 30 - 35
	}
};

const layer_t layer_1 = {
	.keys = 
	{
		{KEY_NORMAL, 0, CLR_NONE},								{KEY_NORMAL, 0, CLR_NONE},				{KEY_NORMAL, 0, CLR_NONE},					{KEY_MACRO, WIN_E, CLR_BLUE},			{KEY_MACRO, CLS_WDW, CLR_BLUE},							{KEY_NORMAL, 0, CLR_NONE},				// 0 - 5
		{KEY_NORMAL, 0, CLR_NONE},								{KEY_NORMAL, 0, CLR_NONE},				{KEY_MACRO, TAB_LFT, CLR_BLUE},				{KEY_NORMAL, HID_UP, CLR_AZUL},			{KEY_MACRO, TAB_RGHT, CLR_BLUE},						{KEY_NORMAL, 0, CLR_NONE},				// 6 - 11
		{KEY_NORMAL, 0, CLR_NONE},								{KEY_MACRO, LEFT_DSKTP, CLR_BLUE},		{KEY_NORMAL, HID_LEFT, CLR_AZUL},			{KEY_NORMAL, HID_DOWN, CLR_AZUL},		{KEY_NORMAL, HID_RIGHT, CLR_AZUL},						{KEY_MACRO, RIGHT_DSKTP, CLR_BLUE},		// 12 - 17
		{KEY_MACRO, WDW_PRV_SCRN, CLR_BLUE},					{KEY_NORMAL, 0, CLR_NONE},				{KEY_MACRO, DEL_DSKTP, CLR_BLUE},			{KEY_MACRO, NEW_DSKTP, CLR_BLUE},		{KEY_NORMAL, 0, CLR_NONE},								{KEY_NORMAL, 0, CLR_NONE},				// 18 - 23 Leftmost function on this row corresponds to button above the Xmega device
		{KEY_MODIFIER, HID_MODIFIER_RIGHT_SHIFT, CLR_GREEN},	{KEY_NORMAL, 0, CLR_NONE},				{KEY_NORMAL, 0, CLR_NONE},					{KEY_NORMAL, 0, CLR_NONE},				{KEY_MODIFIER, HID_MODIFIER_LEFT_CTRL, CLR_GREEN},		{KEY_MACRO, WDW_NXT_SCRN, CLR_BLUE},	// 24 - 29 Rightmost function on this row corresponds to button belowthe Xmega device
		{KEY_LAYER, 0, CLR_RED},								{KEY_NORMAL, HID_ENTER, CLR_GREEN},		{KEY_NORMAL, HID_BACKSPACE, CLR_AZUL},		{KEY_MACRO, WDW_NXT_SLT, CLR_BLUE},		{KEY_MACRO, WDW_PRV_SLT, CLR_BLUE},						{KEY_NORMAL, HID_DELETE, CLR_GREEN}		// 30 - 35
	}
};

#endif

const layer_t* layers[] = {&layer_0, &layer_1};