/*
 * layers.h
 *
 * Created: 3/28/2021 1:17:28 PM
 *  Author: amund
 */ 

#ifndef _MAIN_H_
#define _MAIN_H_

#include "config.h"
#include "leds.h"
#include "layers.h"
#include "conf_usb.h"
#include "ui.h"

void update_current_layer(layer_t*);

void PORT_init(void);

/*! \brief Called by HID interface
 * Callback running when USB Host enable keyboard interface
 *
 * \retval true if keyboard startup is ok
 */
bool main_kbd_enable(void);

/*! \brief Called by HID interface
 * Callback running when USB Host disable keyboard interface
 */
void main_kbd_disable(void);

/*! \brief Called when a start of frame is received on USB line
 */
void main_sof_action(void);

/*! \brief Enters the application in low power mode
 * Callback called when USB host sets USB line in suspend state
 */
void main_suspend_action(void);

/*! \brief Called by UDD when the USB line exit of suspend state
 */
void main_resume_action(void);

/*! \brief Called by UDC when USB Host request to enable remote wakeup
 */
void main_remotewakeup_enable(void);

/*! \brief Called by UDC when USB Host request to disable remote wakeup
 */
void main_remotewakeup_disable(void);

void PORT_init(void);

void reset_key_states(void);

void select_new_layer(void);

void scan_keys(void);

void look_for_updates(void);

void update_key(void);

#endif // _MAIN_H_
