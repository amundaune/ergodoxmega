# ErgodoXmega

Firmware is based on the Microchip ASF Example for USB HID Keyboard for the ATxmega32A4U on the STK600 dev board.

Stripped down most of the code except the USB HID functionality, added custom keyscan based loosely on my Microchip appnote "AN3407 - Using Matrix Keypad With AVR® Devices".
Next step: Add RGB LED functionality which will likely be based off of (or directly use) "sLED library" written by @echoromeo (https://gitlab.com/echoromeo/sLED_lib).

Hardware information is found here: https://workspace.circuitmaker.com/Projects/Details/Amund-Aune-2/ErgoDoxGKA-XMEGA
The keyboard will not be contained in a casing initially. It stands on rubber feet with a bare PCB with parts on it.